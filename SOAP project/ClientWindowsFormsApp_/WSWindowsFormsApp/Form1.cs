﻿using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Web.Services.Description;
using System.Windows.Data;
using System.Windows.Forms;
using WSWindowsFormsApp.EventServiceReference;

namespace WSWindowsFormsApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
        }

        EventServiceReference.EventServiceClient client = new EventServiceClient();
                     

        private void btn_Click(object sender, EventArgs e)
        {


            results.Text = "";
            foreach ( var i in client.getEventsList() )
            {
                results.Text += " nazwa: " + i.name
                    + "\n typ: " + i.type
                    + "\n opis: " + i.description 
                    + "\n data: " + i.date.ToString("dd MMMM yyyy")
                    + "\n rok: " + i.yearNumber.ToString()
                    + "\t miesiac: " +i.monthNumber.ToString()
                    + "\t tydzien roku: " + i.weekNumber.ToString();
                results.Text += "\n\n";
            }

        }
        
        private void btn_week_Click(object sender, EventArgs e)
        {
            results.Text = "";
            try
            {
                int nr = Convert.ToInt32(textBoxWeek.Text);
                foreach (var i in client.getEventsForWeek(nr))
                {
                    results.Text += " nazwa: " + i.name
                        + "\n typ: " + i.type
                        + "\n opis: " + i.description
                        + "\n data: " + i.date.ToString("dd MMMM yyyy")
                        + "\n rok: " + i.yearNumber.ToString()
                        + "\t miesiac: " + i.monthNumber.ToString()
                        + "\t tydzien roku: " + i.weekNumber.ToString();
                    results.Text += "\n\n";
                }
            }
            catch (Exception ex)
            {
                results.Text = "Brak. " + ex.Message;
            }
        }

        private void btn_list_date_Click(object sender, EventArgs e)
        {
            results.Text = "";
            try {
                String data = textBoxDate.Text;
                foreach (var i in client.getEventsForDate(data))
                {
                    results.Text += " nazwa: " + i.name
                        + "\n typ: " + i.type
                        + "\n opis: " + i.description
                        + "\n data: " + i.date.ToString("dd MMMM yyyy")
                        + "\n rok: " + i.yearNumber.ToString()
                        + "\t miesiac: " + i.monthNumber.ToString()
                        + "\t tydzien roku: " + i.weekNumber.ToString();
                    results.Text += "\n\n";
                }
            }
            catch(Exception ex)
            {
                results.Text = "Brak. "+ex.Message;
            }

        }


        private void buttonAddEvent_Click(object sender, EventArgs e)
        {
            using (new OperationContextScope(client.InnerChannel))
            {
                HttpRequestMessageProperty requestMessage = new HttpRequestMessageProperty();
                requestMessage.Headers["Username"] = textBoxLogin.Text;
                requestMessage.Headers["Password"] = textBoxPassword.Text;
                OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = requestMessage;


                results.Text = "";
                try
                {
                    String name = textBoxName.Text;
                    String type = textBoxType.Text;
                    String data = textBoxDateEvent.Text;
                    String desc = textBoxDesc.Text;

                    client.addNewEvent(name, type, data, desc);

                    results.Text += "Dodano nowy event";

                }
                catch (Exception ex)
                {
                    results.Text = "Nie dodano nowego eventu. " + ex.Message;
                }
            }
            
            
        }

        private void btnModifyName_Click(object sender, EventArgs e)
        {
            using (new OperationContextScope(client.InnerChannel))
            {
                HttpRequestMessageProperty requestMessage = new HttpRequestMessageProperty();
                requestMessage.Headers["Username"] = textBoxLogin.Text;
                requestMessage.Headers["Password"] = textBoxPassword.Text;
                OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = requestMessage;


                results.Text = "";
                try
                {
                    String nameModifiedEvent = textBoxNameModifiedEvent.Text;
                    String newValue = textBoxNewName.Text;

                    client.modifyEventName(nameModifiedEvent, newValue);

                    results.Text += "Zmodyfikowano nazwe eventu.";

                }
                catch (Exception ex)
                {
                    results.Text = "Nie zmodyfikowano nazwy eventu. " + ex.Message;
                }
            }

            
        }

        private void btnModifyType_Click(object sender, EventArgs e)
        {
            client = new EventServiceClient();

            using (new OperationContextScope(client.InnerChannel))
            {
                HttpRequestMessageProperty requestMessage = new HttpRequestMessageProperty();
                requestMessage.Headers["Username"] = textBoxLogin.Text;
                requestMessage.Headers["Password"] = textBoxPassword.Text;
                OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = requestMessage;


                results.Text = "";
                try
                {
                    String nameModifiedEvent = textBoxNameModifiedEvent.Text;
                    String newValue = textBoxNewType.Text;

                    client.modifyEventType(nameModifiedEvent, newValue);

                    results.Text += "Zmodyfikowano typ eventu.";

                }
                catch (Exception ex)
                {
                    results.Text = "Nie zmodyfikowano typu eventu. " + ex.Message;
                }
            }

            
        }

        private void btnModifyDate_Click(object sender, EventArgs e)
        {
            using (new OperationContextScope(client.InnerChannel))
            {
                HttpRequestMessageProperty requestMessage = new HttpRequestMessageProperty();
                requestMessage.Headers["Username"] = textBoxLogin.Text;
                requestMessage.Headers["Password"] = textBoxPassword.Text;
                OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = requestMessage;


                results.Text = "";
                try
                {
                    String nameModifiedEvent = textBoxNameModifiedEvent.Text;
                    String newValue = textBoxNewDate.Text;

                    client.modifyEventDate(nameModifiedEvent, newValue);

                    results.Text += "Zmodyfikowano date eventu.";

                }
                catch (Exception ex)
                {
                    results.Text = "Nie zmodyfikowano daty eventu. " + ex.Message;
                }
            }            
        }

        private void btnGetFile_Click(object sender, EventArgs e)
        {
            results.Text = "";
            try
            {
                String filename = textBoxFilename.Text;

                var resultFile = client.fileDownload(filename);

                results.Text += "Pobrano plik.";

            }
            catch (Exception ex)
            {
                results.Text = "Nie pobrano pliku. " + ex.Message;
            }
        }

        private void buttonEventInfo_Click(object sender, EventArgs e)
        {
            results.Text = "";
            try
            {
                String name = textBoxNameInfo.Text;

                var i = client.getEventInfo(name);

                results.Text += " Informacje o evencie\n";

                results.Text += " nazwa: " + i.name
                        + "\n typ: " + i.type
                        + "\n opis: " + i.description
                        + "\n data: " + i.date.ToString("dd MMMM yyyy")
                        + "\n rok: " + i.yearNumber.ToString()
                        + "\t miesiac: " + i.monthNumber.ToString()
                        + "\t tydzien roku: " + i.weekNumber.ToString();
                results.Text += "\n\n";

            }
            catch (Exception ex)
            {
                results.Text = "Nie pobrano informacji na temat takiego eventu.\n" + ex.Message;
            }
        }

       

        private void buttonModifyDesc_Click(object sender, EventArgs e)
        {
            using (new OperationContextScope(client.InnerChannel))
            {
                HttpRequestMessageProperty requestMessage = new HttpRequestMessageProperty();
                requestMessage.Headers["Username"] = textBoxLogin.Text;
                requestMessage.Headers["Password"] = textBoxPassword.Text;
                OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = requestMessage;


                results.Text = "";
                try
                {
                    String nameModifiedEvent = textBoxNameModifiedEvent.Text;
                    String newValue = textBoxNewDesc.Text;

                    client.modifyEventDescription(nameModifiedEvent, newValue);

                    results.Text += "Zmodyfikowano opis eventu.";

                }
                catch (Exception ex)
                {
                    results.Text = "Nie zmodyfikowano opisu eventu. " + ex.Message;
                }
            }
        }
        
    }
}
