﻿namespace WSWindowsFormsApp
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn = new System.Windows.Forms.Button();
            this.results = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_week = new System.Windows.Forms.Button();
            this.btn_list_date = new System.Windows.Forms.Button();
            this.textBoxDate = new System.Windows.Forms.TextBox();
            this.textBoxWeek = new System.Windows.Forms.TextBox();
            this.buttonAddEvent = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxLogin = new System.Windows.Forms.TextBox();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxType = new System.Windows.Forms.TextBox();
            this.textBoxDesc = new System.Windows.Forms.TextBox();
            this.textBoxDateEvent = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxNameModifiedEvent = new System.Windows.Forms.TextBox();
            this.btnModifyName = new System.Windows.Forms.Button();
            this.textBoxNewName = new System.Windows.Forms.TextBox();
            this.btnModifyType = new System.Windows.Forms.Button();
            this.textBoxNewType = new System.Windows.Forms.TextBox();
            this.btnModifyDate = new System.Windows.Forms.Button();
            this.textBoxNewDate = new System.Windows.Forms.TextBox();
            this.buttonEventInfo = new System.Windows.Forms.Button();
            this.textBoxNameInfo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGetFile = new System.Windows.Forms.Button();
            this.textBoxFilename = new System.Windows.Forms.TextBox();
            this.textBoxNewDesc = new System.Windows.Forms.TextBox();
            this.buttonModifyDesc = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn
            // 
            this.btn.Location = new System.Drawing.Point(12, 12);
            this.btn.Name = "btn";
            this.btn.Size = new System.Drawing.Size(227, 35);
            this.btn.TabIndex = 0;
            this.btn.Text = "Pokaż listę eventów";
            this.btn.UseVisualStyleBackColor = true;
            this.btn.Click += new System.EventHandler(this.btn_Click);
            // 
            // results
            // 
            this.results.AutoSize = true;
            this.results.Location = new System.Drawing.Point(413, 34);
            this.results.Name = "results";
            this.results.Size = new System.Drawing.Size(25, 13);
            this.results.TabIndex = 1;
            this.results.Text = "???";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(413, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Wynik:";
            // 
            // btn_week
            // 
            this.btn_week.Location = new System.Drawing.Point(12, 53);
            this.btn_week.Name = "btn_week";
            this.btn_week.Size = new System.Drawing.Size(227, 35);
            this.btn_week.TabIndex = 3;
            this.btn_week.Text = "Pokaż listę eventów w tygodniu:";
            this.btn_week.UseVisualStyleBackColor = true;
            this.btn_week.Click += new System.EventHandler(this.btn_week_Click);
            // 
            // btn_list_date
            // 
            this.btn_list_date.Location = new System.Drawing.Point(12, 94);
            this.btn_list_date.Name = "btn_list_date";
            this.btn_list_date.Size = new System.Drawing.Size(227, 35);
            this.btn_list_date.TabIndex = 4;
            this.btn_list_date.Text = "Pokaż listę eventów w dniu:";
            this.btn_list_date.UseVisualStyleBackColor = true;
            this.btn_list_date.Click += new System.EventHandler(this.btn_list_date_Click);
            // 
            // textBoxDate
            // 
            this.textBoxDate.AllowDrop = true;
            this.textBoxDate.Location = new System.Drawing.Point(245, 102);
            this.textBoxDate.Name = "textBoxDate";
            this.textBoxDate.Size = new System.Drawing.Size(114, 20);
            this.textBoxDate.TabIndex = 5;
            this.textBoxDate.Text = "20-05-2021";
            // 
            // textBoxWeek
            // 
            this.textBoxWeek.Location = new System.Drawing.Point(245, 61);
            this.textBoxWeek.Name = "textBoxWeek";
            this.textBoxWeek.Size = new System.Drawing.Size(114, 20);
            this.textBoxWeek.TabIndex = 6;
            this.textBoxWeek.Text = "19";
            // 
            // buttonAddEvent
            // 
            this.buttonAddEvent.Location = new System.Drawing.Point(82, 310);
            this.buttonAddEvent.Name = "buttonAddEvent";
            this.buttonAddEvent.Size = new System.Drawing.Size(227, 35);
            this.buttonAddEvent.TabIndex = 7;
            this.buttonAddEvent.Text = "Dodaj nowy event";
            this.buttonAddEvent.UseVisualStyleBackColor = true;
            this.buttonAddEvent.Click += new System.EventHandler(this.buttonAddEvent_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(47, 210);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "login:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(202, 210);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "hasło:";
            // 
            // textBoxLogin
            // 
            this.textBoxLogin.Location = new System.Drawing.Point(82, 207);
            this.textBoxLogin.Name = "textBoxLogin";
            this.textBoxLogin.Size = new System.Drawing.Size(114, 20);
            this.textBoxLogin.TabIndex = 11;
            this.textBoxLogin.Text = "user123";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(245, 207);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(114, 20);
            this.textBoxPassword.TabIndex = 12;
            this.textBoxPassword.Text = "password";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 189);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(198, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Dane logowania do modyfikacji eventów";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(19, 258);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(166, 20);
            this.textBoxName.TabIndex = 14;
            this.textBoxName.Text = "nazwa";
            // 
            // textBoxType
            // 
            this.textBoxType.Location = new System.Drawing.Point(193, 258);
            this.textBoxType.Name = "textBoxType";
            this.textBoxType.Size = new System.Drawing.Size(166, 20);
            this.textBoxType.TabIndex = 18;
            this.textBoxType.Text = "typ";
            // 
            // textBoxDesc
            // 
            this.textBoxDesc.Location = new System.Drawing.Point(193, 284);
            this.textBoxDesc.Name = "textBoxDesc";
            this.textBoxDesc.Size = new System.Drawing.Size(166, 20);
            this.textBoxDesc.TabIndex = 20;
            this.textBoxDesc.Text = "opis";
            // 
            // textBoxDateEvent
            // 
            this.textBoxDateEvent.Location = new System.Drawing.Point(19, 284);
            this.textBoxDateEvent.Name = "textBoxDateEvent";
            this.textBoxDateEvent.Size = new System.Drawing.Size(166, 20);
            this.textBoxDateEvent.TabIndex = 19;
            this.textBoxDateEvent.Text = "01-01-2021";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 242);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "Dodawanie eventów";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 359);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "Modyfikacja eventu:";
            // 
            // textBoxNameModifiedEvent
            // 
            this.textBoxNameModifiedEvent.Location = new System.Drawing.Point(125, 356);
            this.textBoxNameModifiedEvent.Name = "textBoxNameModifiedEvent";
            this.textBoxNameModifiedEvent.Size = new System.Drawing.Size(166, 20);
            this.textBoxNameModifiedEvent.TabIndex = 23;
            this.textBoxNameModifiedEvent.Text = "nazwa eventu do modyfikacji";
            // 
            // btnModifyName
            // 
            this.btnModifyName.Location = new System.Drawing.Point(12, 383);
            this.btnModifyName.Name = "btnModifyName";
            this.btnModifyName.Size = new System.Drawing.Size(227, 35);
            this.btnModifyName.TabIndex = 24;
            this.btnModifyName.Text = "Modyfikuj nazwę eventu";
            this.btnModifyName.UseVisualStyleBackColor = true;
            this.btnModifyName.Click += new System.EventHandler(this.btnModifyName_Click);
            // 
            // textBoxNewName
            // 
            this.textBoxNewName.AllowDrop = true;
            this.textBoxNewName.Location = new System.Drawing.Point(245, 391);
            this.textBoxNewName.Name = "textBoxNewName";
            this.textBoxNewName.Size = new System.Drawing.Size(114, 20);
            this.textBoxNewName.TabIndex = 25;
            this.textBoxNewName.Text = "nowa nazwa";
            // 
            // btnModifyType
            // 
            this.btnModifyType.Location = new System.Drawing.Point(12, 424);
            this.btnModifyType.Name = "btnModifyType";
            this.btnModifyType.Size = new System.Drawing.Size(227, 35);
            this.btnModifyType.TabIndex = 26;
            this.btnModifyType.Text = "Modyfikuj typ eventu";
            this.btnModifyType.UseVisualStyleBackColor = true;
            this.btnModifyType.Click += new System.EventHandler(this.btnModifyType_Click);
            // 
            // textBoxNewType
            // 
            this.textBoxNewType.AllowDrop = true;
            this.textBoxNewType.Location = new System.Drawing.Point(245, 432);
            this.textBoxNewType.Name = "textBoxNewType";
            this.textBoxNewType.Size = new System.Drawing.Size(114, 20);
            this.textBoxNewType.TabIndex = 27;
            this.textBoxNewType.Text = "nowy typ";
            // 
            // btnModifyDate
            // 
            this.btnModifyDate.Location = new System.Drawing.Point(12, 465);
            this.btnModifyDate.Name = "btnModifyDate";
            this.btnModifyDate.Size = new System.Drawing.Size(227, 35);
            this.btnModifyDate.TabIndex = 28;
            this.btnModifyDate.Text = "Modyfikuj datę eventu";
            this.btnModifyDate.UseVisualStyleBackColor = true;
            this.btnModifyDate.Click += new System.EventHandler(this.btnModifyDate_Click);
            // 
            // textBoxNewDate
            // 
            this.textBoxNewDate.AllowDrop = true;
            this.textBoxNewDate.Location = new System.Drawing.Point(245, 473);
            this.textBoxNewDate.Name = "textBoxNewDate";
            this.textBoxNewDate.Size = new System.Drawing.Size(114, 20);
            this.textBoxNewDate.TabIndex = 29;
            this.textBoxNewDate.Text = "01-01-2021";
            // 
            // buttonEventInfo
            // 
            this.buttonEventInfo.Location = new System.Drawing.Point(12, 135);
            this.buttonEventInfo.Name = "buttonEventInfo";
            this.buttonEventInfo.Size = new System.Drawing.Size(227, 35);
            this.buttonEventInfo.TabIndex = 30;
            this.buttonEventInfo.Text = "Pokaż info o evencie:";
            this.buttonEventInfo.UseVisualStyleBackColor = true;
            this.buttonEventInfo.Click += new System.EventHandler(this.buttonEventInfo_Click);
            // 
            // textBoxNameInfo
            // 
            this.textBoxNameInfo.AllowDrop = true;
            this.textBoxNameInfo.Location = new System.Drawing.Point(245, 143);
            this.textBoxNameInfo.Name = "textBoxNameInfo";
            this.textBoxNameInfo.Size = new System.Drawing.Size(114, 20);
            this.textBoxNameInfo.TabIndex = 31;
            this.textBoxNameInfo.Text = "nazwa";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 554);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(147, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Pobierz zestawienie jako PDF";
            // 
            // btnGetFile
            // 
            this.btnGetFile.Location = new System.Drawing.Point(12, 570);
            this.btnGetFile.Name = "btnGetFile";
            this.btnGetFile.Size = new System.Drawing.Size(227, 35);
            this.btnGetFile.TabIndex = 33;
            this.btnGetFile.Text = "Pobierz plik PDF o nazwie:";
            this.btnGetFile.UseVisualStyleBackColor = true;
            this.btnGetFile.Click += new System.EventHandler(this.btnGetFile_Click);
            // 
            // textBoxFilename
            // 
            this.textBoxFilename.AllowDrop = true;
            this.textBoxFilename.Location = new System.Drawing.Point(245, 578);
            this.textBoxFilename.Name = "textBoxFilename";
            this.textBoxFilename.Size = new System.Drawing.Size(114, 20);
            this.textBoxFilename.TabIndex = 34;
            this.textBoxFilename.Text = "plikinfo";
            // 
            // textBoxNewDesc
            // 
            this.textBoxNewDesc.AllowDrop = true;
            this.textBoxNewDesc.Location = new System.Drawing.Point(245, 514);
            this.textBoxNewDesc.Name = "textBoxNewDesc";
            this.textBoxNewDesc.Size = new System.Drawing.Size(114, 20);
            this.textBoxNewDesc.TabIndex = 37;
            this.textBoxNewDesc.Text = "nowy opis";
            // 
            // buttonModifyDesc
            // 
            this.buttonModifyDesc.Location = new System.Drawing.Point(12, 506);
            this.buttonModifyDesc.Name = "buttonModifyDesc";
            this.buttonModifyDesc.Size = new System.Drawing.Size(227, 35);
            this.buttonModifyDesc.TabIndex = 36;
            this.buttonModifyDesc.Text = "Modyfikuj opis eventu";
            this.buttonModifyDesc.UseVisualStyleBackColor = true;
            this.buttonModifyDesc.Click += new System.EventHandler(this.buttonModifyDesc_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(830, 615);
            this.Controls.Add(this.textBoxNewDesc);
            this.Controls.Add(this.buttonModifyDesc);
            this.Controls.Add(this.textBoxFilename);
            this.Controls.Add(this.btnGetFile);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxNameInfo);
            this.Controls.Add(this.buttonEventInfo);
            this.Controls.Add(this.textBoxNewDate);
            this.Controls.Add(this.btnModifyDate);
            this.Controls.Add(this.textBoxNewType);
            this.Controls.Add(this.btnModifyType);
            this.Controls.Add(this.textBoxNewName);
            this.Controls.Add(this.btnModifyName);
            this.Controls.Add(this.textBoxNameModifiedEvent);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxDesc);
            this.Controls.Add(this.textBoxDateEvent);
            this.Controls.Add(this.textBoxType);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxPassword);
            this.Controls.Add(this.textBoxLogin);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonAddEvent);
            this.Controls.Add(this.textBoxWeek);
            this.Controls.Add(this.textBoxDate);
            this.Controls.Add(this.btn_list_date);
            this.Controls.Add(this.btn_week);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.results);
            this.Controls.Add(this.btn);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn;
        private System.Windows.Forms.Label results;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_week;
        private System.Windows.Forms.Button btn_list_date;
        private System.Windows.Forms.TextBox textBoxDate;
        private System.Windows.Forms.TextBox textBoxWeek;
        private System.Windows.Forms.Button buttonAddEvent;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxLogin;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxType;
        private System.Windows.Forms.TextBox textBoxDesc;
        private System.Windows.Forms.TextBox textBoxDateEvent;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxNameModifiedEvent;
        private System.Windows.Forms.Button btnModifyName;
        private System.Windows.Forms.TextBox textBoxNewName;
        private System.Windows.Forms.Button btnModifyType;
        private System.Windows.Forms.TextBox textBoxNewType;
        private System.Windows.Forms.Button btnModifyDate;
        private System.Windows.Forms.TextBox textBoxNewDate;
        private System.Windows.Forms.Button buttonEventInfo;
        private System.Windows.Forms.TextBox textBoxNameInfo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnGetFile;
        private System.Windows.Forms.TextBox textBoxFilename;
        private System.Windows.Forms.TextBox textBoxNewDesc;
        private System.Windows.Forms.Button buttonModifyDesc;
    }
}

