/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client2;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import static java.lang.String.valueOf;
import java.net.ProxySelector;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.activation.DataHandler;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.soap.MTOMFeature;

import myservice.Event;
import myservice.EventService;
import myservice.EventServiceService;
import myservice.InvalidInputException_Exception;
import myservice.ParseException_Exception;


public class Client2 {
    
    private static final String WS_URL = "http://localhost:8080/BialystokService/EventServiceService?wsdl";
    public static void main(java.lang.String[] args) throws ParseException_Exception, InvalidInputException_Exception {
             
    ProxySelector.setDefault(new CustomProxySelector());
    
    EventServiceService eventService = new EventServiceService();        
    EventService eventPort = eventService.getEventServicePort(new MTOMFeature());
    
     /*******************UserName & Password ******************************/
        Map<String, Object> req_ctx = ((BindingProvider)eventPort).getRequestContext();
        req_ctx.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, WS_URL);

        Map<String, List<String>> headers = new HashMap<String, List<String>>();
        headers.put("Username", Collections.singletonList("user123"));
        headers.put("Password", Collections.singletonList("password"));
        req_ctx.put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    /**********************************************************************/
    
        
    //lista
    System.out.println("\n-----\nLista 1\n");
    List<Event> eventList = eventPort.getEventsList();
        
    for (int i=0; i<eventList.size(); i++)
      {
          Event u3 = eventList.get(i);
          if (u3 == null) {
              System.out.println("u2 was null");
              System.exit(1);
          }
          System.out.println("\n"+i + ":" + u3.getName() + "\n" + u3.getType() 
                  + "\n" + u3.getDate()+"\n" 
                 +valueOf(u3.getMonthNumber())+" | "+valueOf(u3.getWeekNumber())+" | "+valueOf(u3.getYearNumber() )
                          );
      }
    
//    String filename = ("test");
//    DataHandler dh = eventPort.fileDownload(filename);   
//    filename = ("d:\\generated_pdf\\test.pdf");
//    if (Desktop.isDesktopSupported()) {
//        try {
//            File myFile = new File(filename);
//            Desktop.getDesktop().open(myFile);
//        } catch (IOException ex) {
//            // no application registered for PDFs
//        }
//    }

//try {
//    eventPort.addNewEvent("nowe_wyd", "typ", "23-05-2021", "opis");
//}
//catch (Exception ex) {
//    System.out.print(ex);
//}

//    try {
//   eventPort.addNewEvent("nowe_wyd", "typ", "23-05-2021", "opis");
//    List<Event> eventListw = eventPort.getEventsList();
//        
//    for (int i=0; i<eventListw.size(); i++)
//      {
//          Event u3 = eventListw.get(i);
//          if (u3 == null) {
//              System.out.println("u2 was null");
//              System.exit(1);
//          }
//          System.out.println(i + ": " + u3.getName() + "," + u3.getType() 
//                  + u3.getDate()+" " 
//                 //+valueOf(u3.getMonthNumber())+" "+valueOf(u3.getWeekNumber())+" "+valueOf(u3.getYearNumber() )
//                          );
//      }
//    
//    }
//    catch (InvalidInputException_Exception | ParseException_Exception ex){
//        System.out.print("Exc: " +ex);
//    }
    
//   eventPort.addNewEvent("nowe_wyd", "typ", "23-05-2021", "opis");
    


//    
    
    }
    
}
