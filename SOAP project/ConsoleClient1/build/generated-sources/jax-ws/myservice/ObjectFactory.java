
package myservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the myservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ModifyEventType_QNAME = new QName("http://myservice/", "modifyEventType");
    private final static QName _ModifyEventTypeResponse_QNAME = new QName("http://myservice/", "modifyEventTypeResponse");
    private final static QName _FileDownloadResponse_QNAME = new QName("http://myservice/", "fileDownloadResponse");
    private final static QName _GetEventInfo_QNAME = new QName("http://myservice/", "getEventInfo");
    private final static QName _ParseException_QNAME = new QName("http://myservice/", "ParseException");
    private final static QName _GetEventsList_QNAME = new QName("http://myservice/", "getEventsList");
    private final static QName _ModifyEventDescription_QNAME = new QName("http://myservice/", "modifyEventDescription");
    private final static QName _GetEventsForDateResponse_QNAME = new QName("http://myservice/", "getEventsForDateResponse");
    private final static QName _GetEventsForWeek_QNAME = new QName("http://myservice/", "getEventsForWeek");
    private final static QName _ModifyEventNameResponse_QNAME = new QName("http://myservice/", "modifyEventNameResponse");
    private final static QName _AddNewEvent_QNAME = new QName("http://myservice/", "addNewEvent");
    private final static QName _GetEventsForDate_QNAME = new QName("http://myservice/", "getEventsForDate");
    private final static QName _InvalidInputException_QNAME = new QName("http://myservice/", "InvalidInputException");
    private final static QName _FileDownload_QNAME = new QName("http://myservice/", "fileDownload");
    private final static QName _ModifyEventDescriptionResponse_QNAME = new QName("http://myservice/", "modifyEventDescriptionResponse");
    private final static QName _IsValidUserResponse_QNAME = new QName("http://myservice/", "isValidUserResponse");
    private final static QName _GetEventsForWeekResponse_QNAME = new QName("http://myservice/", "getEventsForWeekResponse");
    private final static QName _ModifyEventDate_QNAME = new QName("http://myservice/", "modifyEventDate");
    private final static QName _AddNewEventResponse_QNAME = new QName("http://myservice/", "addNewEventResponse");
    private final static QName _ModifyEventName_QNAME = new QName("http://myservice/", "modifyEventName");
    private final static QName _GetEventInfoResponse_QNAME = new QName("http://myservice/", "getEventInfoResponse");
    private final static QName _GetEventsListResponse_QNAME = new QName("http://myservice/", "getEventsListResponse");
    private final static QName _IsValidUser_QNAME = new QName("http://myservice/", "isValidUser");
    private final static QName _ModifyEventDateResponse_QNAME = new QName("http://myservice/", "modifyEventDateResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: myservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link InvalidInputException }
     * 
     */
    public InvalidInputException createInvalidInputException() {
        return new InvalidInputException();
    }

    /**
     * Create an instance of {@link FileDownload }
     * 
     */
    public FileDownload createFileDownload() {
        return new FileDownload();
    }

    /**
     * Create an instance of {@link AddNewEvent }
     * 
     */
    public AddNewEvent createAddNewEvent() {
        return new AddNewEvent();
    }

    /**
     * Create an instance of {@link GetEventsForDate }
     * 
     */
    public GetEventsForDate createGetEventsForDate() {
        return new GetEventsForDate();
    }

    /**
     * Create an instance of {@link ModifyEventDescriptionResponse }
     * 
     */
    public ModifyEventDescriptionResponse createModifyEventDescriptionResponse() {
        return new ModifyEventDescriptionResponse();
    }

    /**
     * Create an instance of {@link GetEventsForWeekResponse }
     * 
     */
    public GetEventsForWeekResponse createGetEventsForWeekResponse() {
        return new GetEventsForWeekResponse();
    }

    /**
     * Create an instance of {@link IsValidUserResponse }
     * 
     */
    public IsValidUserResponse createIsValidUserResponse() {
        return new IsValidUserResponse();
    }

    /**
     * Create an instance of {@link GetEventInfoResponse }
     * 
     */
    public GetEventInfoResponse createGetEventInfoResponse() {
        return new GetEventInfoResponse();
    }

    /**
     * Create an instance of {@link GetEventsListResponse }
     * 
     */
    public GetEventsListResponse createGetEventsListResponse() {
        return new GetEventsListResponse();
    }

    /**
     * Create an instance of {@link IsValidUser }
     * 
     */
    public IsValidUser createIsValidUser() {
        return new IsValidUser();
    }

    /**
     * Create an instance of {@link ModifyEventDateResponse }
     * 
     */
    public ModifyEventDateResponse createModifyEventDateResponse() {
        return new ModifyEventDateResponse();
    }

    /**
     * Create an instance of {@link AddNewEventResponse }
     * 
     */
    public AddNewEventResponse createAddNewEventResponse() {
        return new AddNewEventResponse();
    }

    /**
     * Create an instance of {@link ModifyEventName }
     * 
     */
    public ModifyEventName createModifyEventName() {
        return new ModifyEventName();
    }

    /**
     * Create an instance of {@link ModifyEventDate }
     * 
     */
    public ModifyEventDate createModifyEventDate() {
        return new ModifyEventDate();
    }

    /**
     * Create an instance of {@link ModifyEventType }
     * 
     */
    public ModifyEventType createModifyEventType() {
        return new ModifyEventType();
    }

    /**
     * Create an instance of {@link ModifyEventTypeResponse }
     * 
     */
    public ModifyEventTypeResponse createModifyEventTypeResponse() {
        return new ModifyEventTypeResponse();
    }

    /**
     * Create an instance of {@link FileDownloadResponse }
     * 
     */
    public FileDownloadResponse createFileDownloadResponse() {
        return new FileDownloadResponse();
    }

    /**
     * Create an instance of {@link GetEventInfo }
     * 
     */
    public GetEventInfo createGetEventInfo() {
        return new GetEventInfo();
    }

    /**
     * Create an instance of {@link GetEventsList }
     * 
     */
    public GetEventsList createGetEventsList() {
        return new GetEventsList();
    }

    /**
     * Create an instance of {@link ModifyEventDescription }
     * 
     */
    public ModifyEventDescription createModifyEventDescription() {
        return new ModifyEventDescription();
    }

    /**
     * Create an instance of {@link ParseException }
     * 
     */
    public ParseException createParseException() {
        return new ParseException();
    }

    /**
     * Create an instance of {@link GetEventsForWeek }
     * 
     */
    public GetEventsForWeek createGetEventsForWeek() {
        return new GetEventsForWeek();
    }

    /**
     * Create an instance of {@link ModifyEventNameResponse }
     * 
     */
    public ModifyEventNameResponse createModifyEventNameResponse() {
        return new ModifyEventNameResponse();
    }

    /**
     * Create an instance of {@link GetEventsForDateResponse }
     * 
     */
    public GetEventsForDateResponse createGetEventsForDateResponse() {
        return new GetEventsForDateResponse();
    }

    /**
     * Create an instance of {@link Event }
     * 
     */
    public Event createEvent() {
        return new Event();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myservice/", name = "modifyEventType")
    public JAXBElement<ModifyEventType> createModifyEventType(ModifyEventType value) {
        return new JAXBElement<ModifyEventType>(_ModifyEventType_QNAME, ModifyEventType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyEventTypeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myservice/", name = "modifyEventTypeResponse")
    public JAXBElement<ModifyEventTypeResponse> createModifyEventTypeResponse(ModifyEventTypeResponse value) {
        return new JAXBElement<ModifyEventTypeResponse>(_ModifyEventTypeResponse_QNAME, ModifyEventTypeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FileDownloadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myservice/", name = "fileDownloadResponse")
    public JAXBElement<FileDownloadResponse> createFileDownloadResponse(FileDownloadResponse value) {
        return new JAXBElement<FileDownloadResponse>(_FileDownloadResponse_QNAME, FileDownloadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEventInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myservice/", name = "getEventInfo")
    public JAXBElement<GetEventInfo> createGetEventInfo(GetEventInfo value) {
        return new JAXBElement<GetEventInfo>(_GetEventInfo_QNAME, GetEventInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParseException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myservice/", name = "ParseException")
    public JAXBElement<ParseException> createParseException(ParseException value) {
        return new JAXBElement<ParseException>(_ParseException_QNAME, ParseException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEventsList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myservice/", name = "getEventsList")
    public JAXBElement<GetEventsList> createGetEventsList(GetEventsList value) {
        return new JAXBElement<GetEventsList>(_GetEventsList_QNAME, GetEventsList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyEventDescription }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myservice/", name = "modifyEventDescription")
    public JAXBElement<ModifyEventDescription> createModifyEventDescription(ModifyEventDescription value) {
        return new JAXBElement<ModifyEventDescription>(_ModifyEventDescription_QNAME, ModifyEventDescription.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEventsForDateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myservice/", name = "getEventsForDateResponse")
    public JAXBElement<GetEventsForDateResponse> createGetEventsForDateResponse(GetEventsForDateResponse value) {
        return new JAXBElement<GetEventsForDateResponse>(_GetEventsForDateResponse_QNAME, GetEventsForDateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEventsForWeek }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myservice/", name = "getEventsForWeek")
    public JAXBElement<GetEventsForWeek> createGetEventsForWeek(GetEventsForWeek value) {
        return new JAXBElement<GetEventsForWeek>(_GetEventsForWeek_QNAME, GetEventsForWeek.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyEventNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myservice/", name = "modifyEventNameResponse")
    public JAXBElement<ModifyEventNameResponse> createModifyEventNameResponse(ModifyEventNameResponse value) {
        return new JAXBElement<ModifyEventNameResponse>(_ModifyEventNameResponse_QNAME, ModifyEventNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddNewEvent }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myservice/", name = "addNewEvent")
    public JAXBElement<AddNewEvent> createAddNewEvent(AddNewEvent value) {
        return new JAXBElement<AddNewEvent>(_AddNewEvent_QNAME, AddNewEvent.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEventsForDate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myservice/", name = "getEventsForDate")
    public JAXBElement<GetEventsForDate> createGetEventsForDate(GetEventsForDate value) {
        return new JAXBElement<GetEventsForDate>(_GetEventsForDate_QNAME, GetEventsForDate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvalidInputException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myservice/", name = "InvalidInputException")
    public JAXBElement<InvalidInputException> createInvalidInputException(InvalidInputException value) {
        return new JAXBElement<InvalidInputException>(_InvalidInputException_QNAME, InvalidInputException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FileDownload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myservice/", name = "fileDownload")
    public JAXBElement<FileDownload> createFileDownload(FileDownload value) {
        return new JAXBElement<FileDownload>(_FileDownload_QNAME, FileDownload.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyEventDescriptionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myservice/", name = "modifyEventDescriptionResponse")
    public JAXBElement<ModifyEventDescriptionResponse> createModifyEventDescriptionResponse(ModifyEventDescriptionResponse value) {
        return new JAXBElement<ModifyEventDescriptionResponse>(_ModifyEventDescriptionResponse_QNAME, ModifyEventDescriptionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsValidUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myservice/", name = "isValidUserResponse")
    public JAXBElement<IsValidUserResponse> createIsValidUserResponse(IsValidUserResponse value) {
        return new JAXBElement<IsValidUserResponse>(_IsValidUserResponse_QNAME, IsValidUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEventsForWeekResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myservice/", name = "getEventsForWeekResponse")
    public JAXBElement<GetEventsForWeekResponse> createGetEventsForWeekResponse(GetEventsForWeekResponse value) {
        return new JAXBElement<GetEventsForWeekResponse>(_GetEventsForWeekResponse_QNAME, GetEventsForWeekResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyEventDate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myservice/", name = "modifyEventDate")
    public JAXBElement<ModifyEventDate> createModifyEventDate(ModifyEventDate value) {
        return new JAXBElement<ModifyEventDate>(_ModifyEventDate_QNAME, ModifyEventDate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddNewEventResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myservice/", name = "addNewEventResponse")
    public JAXBElement<AddNewEventResponse> createAddNewEventResponse(AddNewEventResponse value) {
        return new JAXBElement<AddNewEventResponse>(_AddNewEventResponse_QNAME, AddNewEventResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyEventName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myservice/", name = "modifyEventName")
    public JAXBElement<ModifyEventName> createModifyEventName(ModifyEventName value) {
        return new JAXBElement<ModifyEventName>(_ModifyEventName_QNAME, ModifyEventName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEventInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myservice/", name = "getEventInfoResponse")
    public JAXBElement<GetEventInfoResponse> createGetEventInfoResponse(GetEventInfoResponse value) {
        return new JAXBElement<GetEventInfoResponse>(_GetEventInfoResponse_QNAME, GetEventInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEventsListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myservice/", name = "getEventsListResponse")
    public JAXBElement<GetEventsListResponse> createGetEventsListResponse(GetEventsListResponse value) {
        return new JAXBElement<GetEventsListResponse>(_GetEventsListResponse_QNAME, GetEventsListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsValidUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myservice/", name = "isValidUser")
    public JAXBElement<IsValidUser> createIsValidUser(IsValidUser value) {
        return new JAXBElement<IsValidUser>(_IsValidUser_QNAME, IsValidUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyEventDateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://myservice/", name = "modifyEventDateResponse")
    public JAXBElement<ModifyEventDateResponse> createModifyEventDateResponse(ModifyEventDateResponse value) {
        return new JAXBElement<ModifyEventDateResponse>(_ModifyEventDateResponse_QNAME, ModifyEventDateResponse.class, null, value);
    }

}
