package myservice;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.jws.WebMethod;
import javax.jws.WebService;

import model.Event;
import java.nio.file.Path;
import java.util.Objects;

import java.io.File;
import java.io.IOException;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.BindingType;
import javax.xml.ws.soap.MTOM;
import javax.xml.ws.soap.SOAPBinding;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.annotation.Resource;
import javax.jws.HandlerChain;
import javax.jws.Oneway;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.soap.SOAPFaultException;

@MTOM(enabled=true, threshold=2048)
@WebService
@BindingType(value = SOAPBinding.SOAP11HTTP_MTOM_BINDING)
public class EventService
{       
    @Resource
    WebServiceContext wsctx;
    
    

    public String isValidUser() {
        
        MessageContext mctx = wsctx.getMessageContext();

        //get detail from request headers
        Map http_headers = (Map) mctx.get(MessageContext.HTTP_REQUEST_HEADERS);
        List userList = (List) http_headers.get("Username");
        List passList = (List) http_headers.get("Password");

        String username = "";
        String password = "";
        
        if(userList!=null){
        	//get username
        	username = userList.get(0).toString();
        }
        	
        if(passList!=null){
        	//get password
        	password = passList.get(0).toString();
        }
        	
        //Should validate username and password with database
        if (username.equals("user123") && password.equals("password")){
        	return "Valid User!";
        }else{
        	return "Unknown User!";
        }
       
    }
    
    List<Event> events = new ArrayList<Event>();    
    Event e;

    public EventService() throws ParseException {
        this.e = new Event("Balet w operze", "kulturalne", "12-05-2021", "Najbardziej znane klasyki w wykonaniu teatru baletowego.");
        events.add(this.e);
        this.e = new Event("Mecz pilki noznej", "sportowe", "22-05-2021", "Mecz na stadionie miejskim, walka o mistrzostwa.");
        events.add(this.e);
        this.e = new Event("Koncert", "kulturalne", "22-05-2021", "Niezwykly koncert skrzypcowy w filharmonii.");
        events.add(this.e);
        this.e = new Event("Wystawa obrazow", "wystawa", "13-05-2021", "Otwarcie wystawy obrazow impresjonistycznych.");
        events.add(this.e);
    }
    
    
    //add new event by passing its name, type, date and description 
    @WebMethod
    public void addNewEvent(String name, String type, String date_string, String description) throws ParseException, InvalidInputException
    {        
        if(isValidUser().equals("Valid User!")) {
            Event e = new Event(name, type, date_string, description);
            this.events.add(e);
        }       
        else {
            String s = isValidUser();
            throw new InvalidInputException("Operacja niedozwolona", s);
        }
    }
    
    //get events for date (in format "dd-MM-yyyy")
    @WebMethod
    public List<Event> getEventsForDate(String date_string) throws ParseException
    {
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        Date date = format.parse(date_string);
                
        List<Event> eventsForDate = new ArrayList<Event>();
        
        for (int i = 0; i < this.events.size(); i++){
                if(this.events.get(i).getDate().equals(date)){
                    eventsForDate.add(this.events.get(i));
                }
            }
        
        return eventsForDate;
    }
    
    //get events for week number
    @WebMethod
    public List<Event> getEventsForWeek(int week) throws ParseException, InvalidInputException
    {               
        if (week <= 52 && week >= 1){
            List<Event> eventsForWeek = new ArrayList<Event>();
        
            for (int i = 0; i < this.events.size(); i++){
                    if(this.events.get(i).getWeekNumber() == week){
                        eventsForWeek.add(this.events.get(i));
                    }
                }

            return eventsForWeek;
        }
        else {
            String s = String.valueOf(week);
            throw new InvalidInputException("Niewlasciwe dane wejsciowe", s+ " nie jest w zakresie 1-52");
        }        
        
    }
    
    //get all events list
    @WebMethod
    public List<Event> getEventsList()
    {     
        return this.events;
    }
        
    
    //get information about event by passing its name
    @WebMethod
    public Event getEventInfo(String name)
    {        
        for (int i = 0; i < this.events.size(); i++){
                if(this.events.get(i).getName().equals(name)){
                    return this.events.get(i);
                }
            }
        return null;
    }
    
    //modify event name
    @WebMethod
    public void modifyEventName(String name, String newName) throws InvalidInputException
    {
        if(isValidUser().equals("Valid User!")) {
            for (int i = 0; i < this.events.size(); i++){
                if(this.events.get(i).getName().equals(name)){
                    this.events.get(i).setName(newName);
                }
            }
        }       
        else {
            String s = isValidUser();
            throw new InvalidInputException("Operacja niedozwolona", s);
        }        
    }
    
    //modify event type
    @WebMethod
    public void modifyEventType(String name, String newType) throws InvalidInputException
    {
        if(isValidUser().equals("Valid User!")) {
            for (int i = 0; i < this.events.size(); i++){
                if(this.events.get(i).getName().equals(name)){
                    this.events.get(i).setType(newType);
                }
            }
        }       
        else {
            String s = isValidUser();
            throw new InvalidInputException("Operacja niedozwolona", s);
        }        
    }
    
    //modify event date by passing string of date (in format "dd.MM.yyyy") 
    @WebMethod
    public void modifyEventDate(String name, String newDate) throws ParseException, InvalidInputException
    {    
        if(isValidUser().equals("Valid User!")) {
            for (int i = 0; i < this.events.size(); i++){
                if(this.events.get(i).getName().equals(name)){
                    this.events.get(i).setDate(newDate);
                }
            }
        }       
        else {
            String s = isValidUser();
            throw new InvalidInputException("Operacja niedozwolona", s);
        } 
        
    }   
    
    //modify event description
    @WebMethod
    public void modifyEventDescription(String name, String newDescription) throws InvalidInputException
    {
        if(isValidUser().equals("Valid User!")) {
            for (int i = 0; i < this.events.size(); i++){
                if(this.events.get(i).getName().equals(name)){
                    this.events.get(i).setDescription(newDescription);
                }
            }
        }       
        else {
            String s = isValidUser();
            throw new InvalidInputException("Operacja niedozwolona", s);
        }        
    }  
             
         
    @XmlMimeType("application/pdf")
    @WebMethod
    public DataHandler fileDownload(String file_name)
    {
        String filename = "d:\\generated_pdf\\" + file_name + ".pdf";
        writeUsingIText(filename);
        return new DataHandler(new FileDataSource(filename));
    }
        
    private void writeUsingIText(String filename) {

        Document document = new Document();

        try {

            PdfWriter.getInstance(document, new FileOutputStream(new File(filename)));

            //open
            document.open();
            
            Font f = new Font();
            f.setStyle(Font.BOLDITALIC);
            f.setSize(18);

            String title = "Zestawienie\n\n";                
            document.add(new Paragraph(title, f));
            document.addTitle(title);
                        
            String info = "";
            String name = "";           
           
            for (int i = 0; i < this.events.size(); i++){        
                
                f.setStyle(Font.BOLD);
                f.setSize(12);
                
                name += "Nazwa: "+ this.events.get(i).getName() + "\n";                
                document.add(new Paragraph(name, f));
                name = "";
                
                f.setStyle(Font.NORMAL);
                f.setSize(10);
                
                info += "Typ wydarzenia: "+ this.events.get(i).getType() + "\n";  
                Date date = this.events.get(i).getDate();  
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");  
                String strDate = dateFormat.format(date);  
                info += "Data: "+ strDate + "\n";
                info += "Tydzien roku: "+ String.valueOf(this.events.get(i).getWeekNumber());
                info += "\t | Miesiac: " + String.valueOf(this.events.get(i).getMonthNumber());
                info += "\t | Rok: " + String.valueOf(this.events.get(i).getYearNumber() + "\n");
                info += "Opis: "+ this.events.get(i).getDescritpion() + "\n\n";
                
                document.add(new Paragraph(info, f));                
                info = "";

            } 

            //close
            document.close();

            System.out.println("New PDF file is done.");
         
        } catch (FileNotFoundException | DocumentException e) {
            e.printStackTrace();
        } 

    }
}