**Projekt WS SOAP**

Temat: Serwis informacyjny Białegostoku

---

## Opis projektu

Wymagania funkcjonalne:

1. Pobieranie eventów dla danego dnia
2. Pobieranie eventów dla danego tygodnia
3. Pobranie informacji o danym evencie
4. Dodawanie eventów
5. Modyfikacja informacji o danym evencie (zmiana nazwy, typu, daty, opisu)
6. Odbiór zestawienia eventów w formacie PDF


### Przeznaczenie
Celem tego dokumentu jest przedstawienie zasad wymiany informacji pomiędzy usługą
sieciową EventServiceService, a oprogramowaniem interfejsowym systemów klienckich,
pobierających informacje dotyczące wydarzeń w Białymstoku oraz umożliwiających ich
modyfikację i dodawanie. Opracowanie przeznaczone jest dla osób i firm z branży IT
przygotowujących samodzielnie oprogramowanie interfejsowe.
### Specyfikacja usługi Web Service
Usługa „EventServiceService” zaimplementowana została jako usługa sieciowa (Web
Service) z użyciem protokołu SOAP w wersji 1.1.
Usługa dostępna jest poprzez protokół HTTP.
### Adres usługi
Projekt po wdrożeniu na serwerze Glassfish jest dostępny pod adresem:
http://localhost:8080/BialystokService/EventServiceService?wsdl


---